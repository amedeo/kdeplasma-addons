# Translation of plasma_applet_notes into Japanese.
# This file is distributed under the same license as the kdeplasma-addons package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2007, 2008, 2009.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-26 01:34+0000\n"
"PO-Revision-Date: 2021-10-17 14:02-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Poedit 2.4.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "外観"

#: package/contents/ui/configAppearance.qml:31
#, kde-format
msgid "%1pt"
msgstr "%1pt"

#: package/contents/ui/configAppearance.qml:37
#, kde-format
msgid "Text font size:"
msgstr "フォントサイズ:"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "A white sticky note"
msgstr "白色の付箋"

#: package/contents/ui/configAppearance.qml:63
#, kde-format
msgid "A black sticky note"
msgstr "黒色の付箋"

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "A red sticky note"
msgstr "赤色の付箋"

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "An orange sticky note"
msgstr "オレンジ色の付箋"

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgid "A yellow sticky note"
msgstr "黄色の付箋"

#: package/contents/ui/configAppearance.qml:67
#, kde-format
msgid "A green sticky note"
msgstr "緑色の付箋"

#: package/contents/ui/configAppearance.qml:68
#, kde-format
msgid "A blue sticky note"
msgstr "青色の付箋"

#: package/contents/ui/configAppearance.qml:69
#, kde-format
msgid "A pink sticky note"
msgstr "ピンク色の付箋"

#: package/contents/ui/configAppearance.qml:70
#, fuzzy, kde-format
#| msgid "A translucent sticky note"
msgid "A transparent sticky note"
msgstr "半透明の付箋"

#: package/contents/ui/configAppearance.qml:71
#, fuzzy, kde-format
#| msgid "A translucent sticky note with light text"
msgid "A transparent sticky note with light text"
msgstr "明るい色のテキストの半透明の付箋"

#: package/contents/ui/main.qml:267
#, kde-format
msgid "Undo"
msgstr "取り消し"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Redo"
msgstr "やり直し"

#: package/contents/ui/main.qml:285
#, kde-format
msgid "Cut"
msgstr "カット"

#: package/contents/ui/main.qml:293
#, kde-format
msgid "Copy"
msgstr "コピー"

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Paste"
msgstr "貼り付け"

#: package/contents/ui/main.qml:307
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgid "Paste with Full Formatting"
msgstr "書式設定なしで貼り付け"

#: package/contents/ui/main.qml:314
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "書式設定なしで貼り付け"

#: package/contents/ui/main.qml:329
#, kde-format
msgid "Delete"
msgstr "削除"

#: package/contents/ui/main.qml:336
#, kde-format
msgid "Clear"
msgstr "クリア"

#: package/contents/ui/main.qml:346
#, kde-format
msgid "Select All"
msgstr "すべて選択"

#: package/contents/ui/main.qml:474
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "太字"

#: package/contents/ui/main.qml:486
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "斜体"

#: package/contents/ui/main.qml:498
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "下線"

#: package/contents/ui/main.qml:510
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "取り消し線"

#: package/contents/ui/main.qml:584
#, kde-format
msgid "Discard this note?"
msgstr "この付箋を破棄しますか？"

#: package/contents/ui/main.qml:585
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "本当にこの付箋を破棄しますか？"

#: package/contents/ui/main.qml:605
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "白"

#: package/contents/ui/main.qml:609
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "黒"

#: package/contents/ui/main.qml:613
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "赤"

#: package/contents/ui/main.qml:617
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "オレンジ"

#: package/contents/ui/main.qml:621
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "黄"

#: package/contents/ui/main.qml:625
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "緑"

#: package/contents/ui/main.qml:629
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "青"

#: package/contents/ui/main.qml:633
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "ピンク"

#: package/contents/ui/main.qml:637
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Transparent"
msgstr "半透明"

#: package/contents/ui/main.qml:641
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Translucent Light"
msgctxt "@item:inmenu"
msgid "Transparent Light"
msgstr "明るい半透明"
